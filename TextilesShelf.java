package bcas.ap.textiles.shelf;

import java.io.*;

public class TextilesShelf {

	public static void printShelfRow(int[] row) {
		for (int s : row) {
			System.out.print(s);
			System.out.print("\t");
		}
	System.out.println();
	}

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int rowColumn2D[][] = new int[5][6];
		int s, t, k = 1;
		int shelfInput;

		for (s = 0; s < 5; s++) {
			for (t = 0; t < 6; t++) {
				rowColumn2D[s][t] = k;
				k++;
			}
		}

		for (int[] row : rowColumn2D) {
			printShelfRow(row);
		}

		for (int l = 0; l < 5; l++) {
			System.out.print("Enter The Number of The Shelf: ");
			shelfInput = Integer.parseInt(br.readLine());
			k = 1;
			
			for (s = 0; s < 5; s++) {
				for (t = 0; t< 6; t++) {
					if (k == shelfInput) {

						if (rowColumn2D[s][t] == 0) {
							System.out.println("That Shelfs has already been reserved");
						} else {
							rowColumn2D[s][t] = 0;
						}
					}
					k++;
				}
			}
			for (int[] row : rowColumn2D) {
				printShelfRow(row);
			}
		}

	}
}
